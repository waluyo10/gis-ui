<?php
namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Terduga;
use Carbon\Carbon;

class TerdugaImportClass implements ToCollection
{
    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        // Skip the header row
        $collection = $collection->slice(1);

        foreach ($collection as $row) {
            // Check if the first column is not empty
            if (!empty($row[0])) {
                // Parse dates from the format 'd/m/Y'
                $ttl = !empty($row[3]) ? Carbon::createFromFormat('d/m/Y', $row[3]) : null;
                $tgl_diagnosis = !empty($row[7]) ? Carbon::createFromFormat('d/m/Y', $row[7]) : null;


                Terduga::create([
                    'id_faskes' => $row[0] ?? null,
                    'nama_petugas' => $row[1] ?? null,
                    'nama_pasien' => $row[2] ?? null,
                    'ttl' => $ttl,
                    'usia' => $row[4] ?? null,
                    'jenis_kelamin' => $row[5] ?? null,
                    'alamat'=> $row[6] ?? null,
                    'tgl_diagnosis' => $tgl_diagnosis,
                    'hasil_lab' => $row[8] ?? null,
                    'riwayat_pengobatan_tb' => $row[9] ?? null,
                    'nama_kader' => $row[10] ?? null,
                    'wilayah_kader' => $row[11] ?? null,
                    'lat' => $row[12] ?? null,
                    'longt' => $row[13] ?? null,
                ]);
            }
        }
    }
}
