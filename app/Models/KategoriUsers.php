<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriUsers extends Model
{
    protected $table = 'kategori_users';

    protected $fillable = [
        'id',
        'kategori',

    ];
}
