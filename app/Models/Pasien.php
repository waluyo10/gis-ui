<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $table = 'tb_pasien';

    protected $fillable = [
        'id',
        'id_faskes',
        'nama_petugas',
        'nama_pasien',
        'ttl',
        'usia',
        'jenis_kelamin',
        'alamat',
        'tgl_diagnosis',
        'tgl_mulai_pengobatan',
        'hasil_pengobatan',
        'nama_kader',
        'wilayah_kader',
        'lat',
        'longt',

    ];
}
