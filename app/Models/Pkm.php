<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pkm extends Model
{
    protected $table = 'pkm';

    protected $fillable = [
        'id',
        'pkm',
        'lat',
        'longt',
        'pkm_id',

    ];
}
