<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Petugas extends Model
{
    protected $table = 'petugas';

    protected $fillable = [
        'id',
        'pkm',
        'nama_petugas',
        'email',

    ];
}
