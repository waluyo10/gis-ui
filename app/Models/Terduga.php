<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Terduga extends Model
{
    protected $table = 'tb_terduga';

    protected $fillable = [
        'id',
        'id_faskes',
        'nama_petugas',
        'nama_pasien',
        'ttl',
        'usia',
        'jenis_kelamin',
        'alamat',
        'tgl_diagnosis',
        'hasil_lab',
        'riwayat_pengobatan_tb',
        'nama_kader',
        'wilayah_kader',
        'lat',
        'longt',

    ];
}
