<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kader extends Model
{
    protected $table = 'kader';

    protected $fillable = [
        'id',
        'wilayah',
        'nama_kader',
        'email',

    ];
}
