<?php

namespace App\Http\Controllers;

use App\Models\Kader;
use App\Models\KategoriUsers;
use App\Models\Pasien;
use App\Models\Petugas;
use App\Models\Pkm;
use App\Models\Terduga;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Imports\PasienImportClass;
use App\Imports\TerdugaImportClass;
use Maatwebsite\Excel\Facades\Excel;

class RoutingController extends Controller
{


    // public function __construct()
    // {
    //     $this->middleware('auth')->except('index');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terdugacount = Terduga::count();
        $pasiencount = Pasien::count();
        $Pkms = Pkm::orderBy('pkm', 'asc')->get();
        return view('index', compact('terdugacount','pasiencount','Pkms'));
    }

    /**
     * Display a view based on first route param
     *
     * @return \Illuminate\Http\Response
     */
    public function root($first)
    {
        if ($first != 'assets')
            return view($first);
        return view('index');
    }

    /**
     * second level route
     */
    public function secondLevel($first, $second, Request $request)
    {
        if($first == 'pasien' && $second == 'data'){
            $datas = Pasien::orderBy('tgl_mulai_pengobatan', 'desc')->get();
            return view('pasien.data',
            compact('datas'));
        }
        if($first == 'terduga' && $second == 'data'){
            $datas = Terduga::orderBy('tgl_mulai_pengobatan', 'desc')->get();
            return view('terduga.data',
            compact('datas'));
        }
        if($first == 'profil' && $second == 'data'){
            $datas = Kader::orderBy('nama_kader', 'asc')->get();
            $datapetugass = Petugas::orderBy('nama_petugas', 'asc')->get();
            return view('profil.data',
            compact('datas','datapetugass'));
        }
        if($first == 'master' && $second == 'data'){
            $datas = User::orderBy('name', 'asc')->get();
            return view('master.data',
            compact('datas'));
        }
        if($first == 'pkm' && $second == 'data'){
            $datas = Pkm::orderBy('pkm_id', 'asc')->get();
            return view('pkm.data',
            compact('datas'));
        }

        if($first == 'pasien' && $second == 'simpandata'){
          $simpanData = new Pasien();
          $simpanData->id_faskes = $request->id_faskes;
          $simpanData->nama_petugas = $request->nama_petugas;
          $simpanData->nama_pasien = $request->nama_pasien;
          $simpanData->ttl = $request->ttl;
          $simpanData->usia = $request->usia;
          $simpanData->jenis_kelamin = $request->jenis_kelamin;
          $simpanData->alamat = $request->alamat;
          $simpanData->lat = $request->lat;
          $simpanData->longt = $request->longt;
          $simpanData->tgl_diagnosis = $request->tgl_diagnosis;
          $simpanData->tgl_mulai_pengobatan = $request->tgl_mulai_pengobatan;
          $simpanData->hasil_pengobatan = $request->hasil_pengobatan;
          $simpanData->nama_kader = $request->nama_kader;
          $simpanData->wilayah_kader = $request->wilayah_kader;
          $simpanData->save();
          return redirect()->route('second', ['pasien', 'data'])
                ->with('success', 'Data berhasil disimpan.')
                ->with('showModalinfo', true);
        }

        if($first == 'pasien' && $second == 'editdatatitik'){

            $record = Pasien::where('id', $request->id)->first();
            $record->lat = $request->lat_titik;
            $record->longt = $request->longt_titik;
            $record->save();
            return redirect()->route('second', ['pasien', 'data'])
                  ->with('success', 'Kordinat berhasil di ubah.')
                  ->with('showModalinfo', true);

        }

        if($first == 'terduga' && $second == 'simpandata'){
            $simpanData = new Terduga();
            $simpanData->id_faskes = $request->id_faskes;
            $simpanData->nama_petugas = $request->nama_petugas;
            $simpanData->nama_pasien = $request->nama_pasien;
            $simpanData->ttl = $request->ttl;
            $simpanData->usia = $request->usia;
            $simpanData->jenis_kelamin = $request->jenis_kelamin;
            $simpanData->alamat = $request->alamat;
            $simpanData->lat = $request->lat;
            $simpanData->longt = $request->longt;
            $simpanData->tgl_diagnosis = $request->tgl_diagnosis;
            $simpanData->tgl_mulai_pengobatan = $request->tgl_mulai_pengobatan;
            $simpanData->hasil_pengobatan = $request->hasil_pengobatan;
            $simpanData->nama_kader = $request->nama_kader;
            $simpanData->wilayah_kader = $request->wilayah_kader;
            $simpanData->save();
            return redirect()->route('second', ['terduga', 'data'])
                  ->with('success', 'Data berhasil disimpan.')
                  ->with('showModalinfo', true);
          }


          if($first == 'kader' && $second == 'simpandata'){
            $cekData = User::where('email', $request->email)->first();
            if($cekData){
                $cekData->kat_id_user = 3;
                $cekData->save();
            }else{
                return redirect()->route('second', ['profil', 'data'])
                ->with('error', 'Email tidak valid pastikan email sama saat registrasi !!');
            }

            $simpanData = new Kader();
            $simpanData->nama_kader = $request->nama_kader;
            $simpanData->wilayah = $request->wilayah;
            $simpanData->email = $request->email;
            $simpanData->save();
            return redirect()->route('second', ['profil', 'data'])
                  ->with('success', 'Data berhasil disimpan.')
                  ->with('showModalinfo', true);

        }

        if($first == 'terduga' && $second == 'editdatatitik'){

            $record = Terduga::where('id', $request->id)->first();
            $record->lat = $request->lat_titik;
            $record->longt = $request->longt_titik;
            $record->save();
            return redirect()->route('second', ['terduga', 'data'])
                  ->with('success', 'Kordinat berhasil di ubah.')
                  ->with('showModalinfo', true);

        }

        if($first == 'users' && $second == 'simpandata'){
            $simpanData = User::where('id', $request->id)->first();
            $simpanData->kat_id_user = $request->kategori;
            $simpanData->save();
            return redirect()->route('second', ['master', 'data'])
                  ->with('success', 'Data berhasil disimpan.')
                  ->with('showModalinfo', true);

        }
        if($first == 'petugas' && $second == 'simpandata'){
            $cekData = User::where('email', $request->email)->first();
            if($cekData){
                $cekData->kat_id_user = 2;
                $cekData->save();
            }else{
                return redirect()->route('second', ['profil', 'data'])
                ->with('error', 'Email tidak valid pastikan email sama saat registrasi !!');
            }

            $simpanData = new Petugas();
            $simpanData->pkm = $request->pkm;
            $simpanData->nama_petugas = $request->nama_petugas;
            $simpanData->email = $request->email;
            $simpanData->save();
            return redirect()->route('second', ['profil', 'data'])
            ->with('success', 'Data berhasil disimpan.')
            ->with('showModalinfo', true);
        }
        if($first == 'pkm' && $second == 'simpandata'){
            $simpanData = new Pkm();
            $simpanData->pkm = $request->pkm;
            $simpanData->pkm_id = $request->pkm_id;
            $simpanData->lat = $request->lat;
            $simpanData->longt = $request->longt;
            $simpanData->save();
            return redirect()->route('second', ['pkm', 'data'])
            ->with('success', 'Data berhasil disimpan.')
            ->with('showModalinfo', true);
        }
        if($first == 'kaderdelete'){
            $id = decrypt($second);
            $record = Kader::find($id);

            if ($record) {
                $record->delete();
                return redirect()->route('second', ['profil', 'data'])->with('success', 'Data dengan ID ' . $id . ' telah dihapus.');
            } else {
                return redirect()->route('second', ['profil', 'data'])->with('error', 'Data dengan ID ' . $id . ' tidak ditemukan.');
            }

        }
        if($first == 'terdugaDelete'){
            $id = decrypt($second);

            $record = Terduga::where('id', $id)->first();

            if (!$record) {
                return redirect()->back()->with('error', 'Data dengan ID ' . $id . ' tidak ditemukan.');
            }
            $record->delete();
            return redirect()->back()->with('success', 'Data dengan ID ' . $id . ' telah dihapus.');
        }
        if($first == 'petugasdelete'){
            $id = decrypt($second);
            $record = Petugas::find($id);

            if ($record) {
                $record->delete();
                return redirect()->route('second', ['profil', 'data'])->with('success', 'Data dengan ID ' . $id . ' telah dihapus.');
            } else {
                return redirect()->route('second', ['profil', 'data'])->with('error', 'Data dengan ID ' . $id . ' tidak ditemukan.');
            }

        }
        if($first == 'pasiendelete'){
            $id = decrypt($second);

            $record = Pasien::where('id', $id)->first();

            if (!$record) {
                return redirect()->back()->with('error', 'Data dengan ID ' . $id . ' tidak ditemukan.');
            }
            $record->delete();
            return redirect()->back()->with('success', 'Data dengan ID ' . $id . ' telah dihapus.');

        }
        if($first == 'pkmdelete'){
            $id = decrypt($second);
            $record = Pkm::where('id', $id)->first();

            if ($record) {
                $record->delete();
                return redirect()->route('second', ['pkm', 'data'])->with('success', 'Data dengan ID ' . $id . ' telah dihapus.');
            } else {
                return redirect()->route('second', ['pkm', 'data'])->with('error', 'Data dengan ID ' . $id . ' tidak ditemukan.');
            }

        }
        if($first == 'usersdelete'){
            $id = decrypt($second);
            $record = User::where('id', $id)->first();

            if ($record) {
                $record->delete();
                return redirect()->route('second', ['master', 'data'])->with('success', 'Data dengan ID ' . $id . ' telah dihapus.');
            } else {
                return redirect()->route('second', ['master', 'data'])->with('error', 'Data dengan ID ' . $id . ' tidak ditemukan.');
            }

        }
        if($first == 'import' && $second == 'pasien'){
            $file = $request->file('file');

            Excel::import(new PasienImportClass, $file);

            return redirect()->back()->with('success', 'Data berhasil di import!');
                }
        if($first == 'import' && $second == 'terduga'){
            $file = $request->file('file');

            Excel::import(new TerdugaImportClass, $file);

            return redirect()->back()->with('success', 'Data berhasil di import!');
            }

          if ($first != 'assets')
            return view($first . '.' . $second);
        return view('index');
    }

    /**
     * third level route
     */
    public function thirdLevel($first, $second, $third, Request $request)
    {
        //get data json
        if($first == 'pasienterduga' && $second == 'data' && $third == 'all'){
            // Earth radius in kilometers
            $earthRadius = 6371;

            // Fetch data for 'Pasien'
            $pasienData = DB::table('tb_pasien')
                ->join('pkm', 'pkm.pkm_id', '=', 'tb_pasien.id_faskes')
                ->select(
                    'nama_pasien',
                    'tb_pasien.lat as lat',
                    'tb_pasien.longt as lng',
                    DB::raw("'red' as color"),
                    DB::raw("'Pasien' as status"),
                    'pkm.pkm as pkm',
                    'tb_pasien.hasil_pengobatan as hasil_pengobatan',
                    DB::raw("ROUND($earthRadius * ACOS(COS(RADIANS(tb_pasien.lat)) * COS(RADIANS(pkm.lat)) * COS(RADIANS(pkm.longt) - RADIANS(tb_pasien.longt)) + SIN(RADIANS(tb_pasien.lat)) * SIN(RADIANS(pkm.lat))), 2) AS jarak")
                )
                ->whereNotNull('tb_pasien.lat')
                ->whereNotNull('tb_pasien.longt')
                ->get();

            // Fetch data for 'Terduga'
            $terdugaData = DB::table('tb_terduga')
                ->join('pkm', 'pkm.pkm_id', '=', 'tb_terduga.id_faskes')
                ->select(
                    'nama_pasien',
                    'tb_terduga.lat as lat',
                    'tb_terduga.longt as lng',
                    DB::raw("'blue' as color"),
                    DB::raw("'Terduga' as status"),
                    'pkm.pkm as pkm',
                    DB::raw("'-' as hasil_pengobatan"),
                    DB::raw("ROUND($earthRadius * ACOS(COS(RADIANS(tb_terduga.lat)) * COS(RADIANS(pkm.lat)) * COS(RADIANS(pkm.longt) - RADIANS(tb_terduga.longt)) + SIN(RADIANS(tb_terduga.lat)) * SIN(RADIANS(pkm.lat))), 2) AS jarak")
                )
                ->whereNotNull('tb_terduga.lat')
                ->whereNotNull('tb_terduga.longt')
                ->get();

            // Combine both sets of data
            $combinedData = $pasienData->concat($terdugaData);

            return response()->json(['data' => $combinedData]);
        }
        if($first == 'pasienterduga' && $second == 'data' && $third == 'heatmap'){
                    // Fetch data for 'Pasien'
        $pasienData = DB::table('tb_pasien')
        ->select('lat as lat', 'longt as lng', DB::raw("'0.7' as color"))
        ->whereNotNull('lat')
        ->whereNotNull('longt')
        ->get();

        // Fetch data for 'Terduga'
        $terdugaData = DB::table('tb_terduga')
        ->select('lat as lat', 'longt as lng', DB::raw("'0.3' as color"))
        ->whereNotNull('lat')
        ->whereNotNull('longt')
        ->get();

        // Combine both sets of data
        $combinedData = $pasienData->concat($terdugaData);

        // Transform the data into the desired format
        $heatmapData = $combinedData->map(function ($item) {
        return [(float) $item->lat, (float) $item->lng, (float) $item->color];
        });

        // Return the response
        return response()->json(['data' => $heatmapData]);


            }
        //pkm data
        if($first == 'pkm' && $second == 'data' && $third == 'all'){
            $results = DB::table('pkm as p')
    ->select(
        'p.pkm',
        'p.pkm_id',
        DB::raw('COALESCE(tp.pasien_count, 0) AS total_pasien'),
        DB::raw('COALESCE(tt.terduga_count, 0) AS total_terduga'),
        DB::raw('COALESCE(ts.sembuh_count, 0) AS total_sembuh'),
        DB::raw('IF(COALESCE(tp.pasien_count, 0) > 0, ROUND((COALESCE(ts.sembuh_count, 0) / COALESCE(tp.pasien_count, 0)) * 100, 2), 0) AS persentase_sembuh'),
        'p.lat',
        'p.longt as lng'
    )
    ->leftJoin(DB::raw('(SELECT id_faskes, COUNT(id) AS pasien_count FROM tb_pasien GROUP BY id_faskes) as tp'), 'p.pkm_id', '=', 'tp.id_faskes')
    ->leftJoin(DB::raw('(SELECT id_faskes, COUNT(id) AS terduga_count FROM tb_terduga GROUP BY id_faskes) as tt'), 'p.pkm_id', '=', 'tt.id_faskes')
    ->leftJoin(DB::raw('(SELECT id_faskes, COUNT(id) AS sembuh_count FROM tb_pasien WHERE hasil_pengobatan = "sembuh" GROUP BY id_faskes) as ts'), 'p.pkm_id', '=', 'ts.id_faskes')
    ->get();

return response()->json(['data' => $results]);


        }
         //pkm data
         if($first == 'pkm' && $second == 'caridata'){
            $results = DB::table('pkm as p')
    ->select(
        'p.pkm',
        'p.pkm_id',
        DB::raw('COALESCE(tp.pasien_count, 0) AS total_pasien'),
        DB::raw('COALESCE(tt.terduga_count, 0) AS total_terduga'),
        DB::raw('COALESCE(ts.sembuh_count, 0) AS total_sembuh'),
        DB::raw('IF(COALESCE(tp.pasien_count, 0) > 0, ROUND((COALESCE(ts.sembuh_count, 0) / COALESCE(tp.pasien_count, 0)) * 100, 2), 0) AS persentase_sembuh'),
        'p.lat',
        'p.longt as lng'
    )
    ->leftJoin(DB::raw('(SELECT id_faskes, COUNT(id) AS pasien_count FROM tb_pasien GROUP BY id_faskes) as tp'), 'p.pkm_id', '=', 'tp.id_faskes')
    ->leftJoin(DB::raw('(SELECT id_faskes, COUNT(id) AS terduga_count FROM tb_terduga GROUP BY id_faskes) as tt'), 'p.pkm_id', '=', 'tt.id_faskes')
    ->leftJoin(DB::raw('(SELECT id_faskes, COUNT(id) AS sembuh_count FROM tb_pasien WHERE hasil_pengobatan = "sembuh" GROUP BY id_faskes) as ts'), 'p.pkm_id', '=', 'ts.id_faskes')
            ->where('p.pkm_id', $third)
            ->get();
            return response()->json(['data' => $results]);
        }

        if($first == 'pasien' && $second == 'data' && $third == 'all'){
            $earthRadius = 6371;
            $pasienData = DB::table('tb_pasien')
            ->join('pkm', 'pkm.pkm_id', '=', 'tb_pasien.id_faskes')
            ->select(
                'nama_pasien',
                'tb_pasien.lat as lat',
                'tb_pasien.longt as lng',
                DB::raw("'red' as color"),
                DB::raw("'Pasien' as status"),
                'pkm.pkm as pkm',
                'tb_pasien.hasil_pengobatan as hasil_pengobatan',
                DB::raw("ROUND($earthRadius * ACOS(COS(RADIANS(tb_pasien.lat)) * COS(RADIANS(pkm.lat)) * COS(RADIANS(pkm.longt) - RADIANS(tb_pasien.longt)) + SIN(RADIANS(tb_pasien.lat)) * SIN(RADIANS(pkm.lat))), 2) AS jarak")
            )
            ->whereNotNull('tb_pasien.lat')
            ->whereNotNull('tb_pasien.longt')
            ->get();

            return response()->json(['data' => $pasienData]);

            }
        if($first == 'terduga' && $second == 'data' && $third == 'all'){
                // Fetch data for 'Terduga'
                $earthRadius = 6371;
                $terdugaData = DB::table('tb_terduga')
                ->join('pkm', 'pkm.pkm_id', '=', 'tb_terduga.id_faskes')
                ->select(
                    'nama_pasien',
                    'tb_terduga.lat as lat',
                    'tb_terduga.longt as lng',
                    DB::raw("'blue' as color"),
                    DB::raw("'Terduga' as status"),
                    'pkm.pkm as pkm',
                    DB::raw("'-' as hasil_pengobatan"),
                    DB::raw("ROUND($earthRadius * ACOS(COS(RADIANS(tb_terduga.lat)) * COS(RADIANS(pkm.lat)) * COS(RADIANS(pkm.longt) - RADIANS(tb_terduga.longt)) + SIN(RADIANS(tb_terduga.lat)) * SIN(RADIANS(pkm.lat))), 2) AS jarak")
                )
                ->whereNotNull('tb_terduga.lat')
                ->whereNotNull('tb_terduga.longt')
                ->get();
                return response()->json(['data' => $terdugaData]);

        }
        if($first == 'users' && $second == 'detail'){
            $id = decrypt($third);
            $datas = User::orderBy('name', 'asc')->get();
            $userdetails = User::where('id', $id)->get();;
            $katUsers = KategoriUsers::orderBy('id', 'asc')->get();

            return view('master.data',
            compact('datas','userdetails','katUsers'))
            ->with('showModal', true);

        }
        if($first == 'terduga' && $second == 'detail'){
            $id = decrypt($third);
            $terdugaDetails = Terduga::where('id', $id)->first();
            $Pkms = Pkm::orderBy('pkm', 'asc')->get();
            $Petugass = Petugas::orderBy('nama_petugas', 'asc')->get();
            $Kaders = Kader::orderBy('nama_kader', 'asc')->get();

            return view('terduga.edit',
            compact('terdugaDetails','Pkms','Petugass','Kaders'))
            ->with('showModal', true);


        }
        if($first == 'pasien' && $second == 'detail'){
            $id = decrypt($third);
            $pasienDetails = Pasien::where('id', $id)->first();
            $Pkms = Pkm::orderBy('pkm', 'asc')->get();
            $Petugass = Petugas::orderBy('nama_petugas', 'asc')->get();
            $Kaders = Kader::orderBy('nama_kader', 'asc')->get();

            return view('pasien.edit',
            compact('pasienDetails','Pkms','Petugass','Kaders'))
            ->with('showModal', true);


        }
        if($first == 'terduga' && $second == 'editdata'){
            $simpanData = Terduga::where('id', $third)->first();
            $simpanData->id_faskes = $request->id_faskes;
            $simpanData->nama_petugas = $request->nama_petugas;
            $simpanData->nama_pasien = $request->nama_pasien;
            $simpanData->ttl = $request->ttl;
            $simpanData->usia = $request->usia;
            $simpanData->jenis_kelamin = $request->jenis_kelamin;
            $simpanData->alamat = $request->alamat;
            $simpanData->lat = $request->lat;
            $simpanData->longt = $request->longt;
            $simpanData->tgl_diagnosis = $request->tgl_diagnosis;
            $simpanData->tgl_mulai_pengobatan = $request->tgl_mulai_pengobatan;
            $simpanData->hasil_pengobatan = $request->hasil_pengobatan;
            $simpanData->nama_kader = $request->nama_kader;
            $simpanData->wilayah_kader = $request->wilayah_kader;
            $simpanData->save();

            return redirect()->route('second', ['terduga', 'data'])
                  ->with('success', 'Data berhasil disimpan.')
                  ->with('showModalinfo', true);
          }

          if($first == 'pasien' && $second == 'editdata'){
            $simpanData = Pasien::where('id', $third)->first();
            $simpanData->id_faskes = $request->id_faskes;
            $simpanData->nama_petugas = $request->nama_petugas;
            $simpanData->nama_pasien = $request->nama_pasien;
            $simpanData->ttl = $request->ttl;
            $simpanData->usia = $request->usia;
            $simpanData->jenis_kelamin = $request->jenis_kelamin;
            $simpanData->alamat = $request->alamat;
            $simpanData->lat = $request->lat;
            $simpanData->longt = $request->longt;
            $simpanData->tgl_diagnosis = $request->tgl_diagnosis;
            $simpanData->tgl_mulai_pengobatan = $request->tgl_mulai_pengobatan;
            $simpanData->hasil_pengobatan = $request->hasil_pengobatan;
            $simpanData->nama_kader = $request->nama_kader;
            $simpanData->wilayah_kader = $request->wilayah_kader;
            $simpanData->save();

            return redirect()->route('second', ['pasien', 'data'])
                  ->with('success', 'Data berhasil disimpan.')
                  ->with('showModalinfo', true);
          }


        if ($first != 'assets')
            return view($first . '.' . $second . '.' . $third);
        return view('index');
    }
}
