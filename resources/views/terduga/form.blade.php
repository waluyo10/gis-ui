@extends('layouts.vertical', ['title' => 'Terduga Pasien TB Ro'])
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet-draw/dist/leaflet.draw.css" />
@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Menu</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Terduga Pasien TB RO</a></li>
                        </ol>
                    </div>
                    <h4 class="page-title">Terduga Pasien TB RO</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="header-title text-center">Form Tambah Data</h4>
                    <form id="myForm" action="{{route('second', ['terduga', 'simpandata'])}}" method="POST" enctype="multipart/form-data" >
                    @csrf
                    <div class="modal-body p-4">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Nama Faskes<span class="text-danger">*</span></label>
                                    <input type="text" name="id_faskes" class="form-control" id="id_faskes" maxlength="250" placeholder="Nama Faskes" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Nama Petugas<span class="text-danger">*</span></label>
                                    <select class="form-control" id="nama_petugas" name="nama_petugas">
                                        <option value="">--Pilih Nama Petugas--</option>
                                    @php
                                        $namaPetugas = \App\Models\Petugas::orderBy('nama_petugas', 'asc')->get();

                                        if ($namaPetugas->isNotEmpty()) {
                                        foreach ($namaPetugas as $namaPetuga) {
                                        echo '<option value="' . $namaPetuga->nama_petugas . '">' . $namaPetuga->nama_petugas  . '</option>';
                                        }
                                        } else {
                                        echo '<option value="-">-</option>';
                                        }
                                        @endphp
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Nama Terduga Pasien<span class="text-danger">*</span></label>
                                    <input type="text" name="nama_pasien" class="form-control" id="nama_pasien" maxlength="250" placeholder="Nama Terduga Pasien" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">TTL<span class="text-danger">*</span></label>
                                    <input type="date" id="ttl" name="ttl" class="form-control" placeholder="Pilih Tanggal" required />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Usia<span class="text-danger">*</span></label>
                                    <input type="number" id="usia" name="usia" class="form-control" placeholder="Usia" required />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                <label for="field-1" class="control-label">Jenis Kelamin<span class="text-danger">*</span></label>
                                    <select class="form-control" id="jenis_kelamin" name="jenis_kelamin" required>
                                        <option value="">--Pilih Jenis Kelamin--</option>
                                        <option value="L">Laki Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Alamat Pasien<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="alamat" id="alamat" placeholder="Alamat" rows="3" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-2" class="control-label">Lat<span class="text-danger">*</span></label>
                                    <input type="text" name="lat" class="form-control" id="lat" placeholder="Lat" required >
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-2" class="control-label">Longt<span class="text-danger">*</span></label>
                                    <input type="text" name="longt" class="form-control" placeholder="Longt" id="longt" required >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                <div id="map" style="height: 200px;"></div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-2" class="control-label">Tanggal Diagnosisi<span class="text-danger">*</span></label>
                                    <input type="date" name="tgl_diagnosis" class="form-control" id="tgl_diagnosis" placeholder="Pilih Tanggal" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-2" class="control-label">Tanggal mulai pengobatan<span class="text-danger">*</span></label>
                                    <input type="date" name="tgl_mulai_pengobatan" class="form-control" placeholder="Pilih Tanggal" id="tgl_mulai_pengobatan" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Hasil Pengobatan<span class="text-danger">*</span></label>
                                    <input type="text" name="hasil_pengobatan" class="form-control" id="hasil_pengobatan" maxlength="250" placeholder="Hasil Pengobatan" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-1" class="control-label">Nama Kader<span class="text-danger">*</span></label>
                                    <select class="form-control" id="nama_kader" name="nama_kader">
                                        <option value="">--Pilih Nama Kader--</option>
                                    @php
                                        $namaKaders = \App\Models\Kader::orderBy('nama_kader', 'asc')->get();

                                        if ($namaKaders->isNotEmpty()) {
                                        foreach ($namaKaders as $namaKader) {
                                        echo '<option value="' . $namaKader->nama_kader . '">' . $namaKader->nama_kader  . '</option>';
                                        }
                                        } else {
                                        echo '<option value="-">-</option>';
                                        }
                                        @endphp
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Wilayah Kader<span class="text-danger">*</span></label>
                                    <input type="text" name="wilayah_kader" class="form-control" id="wilayah_kader" maxlength="250" placeholder="Wilayah kader" required/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                    </div>
                </form>
            </div> <!-- end col -->
        </div>
        <!-- end row -->
        <!-- sample modal content -->




    </div> <!-- container -->

@endsection

@section('script')
    <!-- Plugins js-->
    <script src="{{asset('assets/libs/footable/footable.min.js')}}"></script>

    <!-- Page js-->
    <script src="{{asset('assets/js/pages/foo-tables.init.js')}}"></script>

    <script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-draw"></script>

    <script>
   var map = L.map('map').setView([-6.2088, 106.8456], 12);
 // Set view to DKI Jakarta

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
    }).addTo(map);

    var drawnItems = new L.FeatureGroup();
    map.addLayer(drawnItems);

    var drawControl = new L.Control.Draw({
        draw: {
            marker: true,
            circle: false,
            rectangle: false,
            polygon: false,
            polyline: false,
        },
        edit: {
            featureGroup: drawnItems,
            remove: true,
        },
    });
    map.addControl(drawControl);

    map.on(L.Draw.Event.CREATED, function (e) {
        var layer = e.layer;
        drawnItems.addLayer(layer);

        var latlng = layer.getLatLng();
        document.getElementById('lat').value = latlng.lat;
        document.getElementById('longt').value = latlng.lng;
    });
</script>





@endsection
