@extends('layouts.vertical', ['title' => 'Peta Sebaran'])
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet-draw/dist/leaflet.draw.css" />
@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Menu</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Peta Sebaran</a></li>
                        </ol>
                    </div>
                    <h4 class="page-title">Peta Sebaran Kasus TB RO</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="card-box">
            <div class="card-body">
                <div class="form-row">
            <div class="col-md-6 mb-2">
            <label for="status" class="control-label">Pencarian Berdasarkan Status:</label>
            <select class="custom-select" id="statuscek" onchange="cari()">
                <option value="1">Semua</option>
                <option value="2">Pasien</option>
                <option value="3">Terduga</option>
            </select>
            </div>
            </div>
            </div>
            <div class="col-md-12 mb-2 text-center">
               <button type="button" class="btn btn-danger mt-2 mr-2 ml-2">
                Jumlah <span class="text-white " id="pasiencek"></span>
                <button type="button" class="btn btn-blue mt-2">
                Jumlah <span class="text-white" id="terdugacek"></span>
            </div>
        </div>
        <div class="card-box">
        <div class="text-center"><h3>Detail Pasien & Terduga</h3></div>
        <div id="map" style="height: 1000px;"></div>
        </div>
        <div class="card-box">
            <div class="text-center"><h3>Heatmap Lokasi Sebaran Jarak</h3></div>
        <div id="mapheat" style="height: 1000px;"></div>
        </div>
        </div>


@endsection

@section('script')
    <!-- Plugins js-->
    <script src="{{asset('assets/libs/footable/footable.min.js')}}"></script>

    <!-- Page js-->
    <script src="{{asset('assets/js/pages/foo-tables.init.js')}}"></script>

    <script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-draw"></script>
    <!-- heat-->
    <script src="https://unpkg.com/leaflet.heat/dist/leaflet-heat.js"></script>

<script>
  let map = null;

document.addEventListener('DOMContentLoaded', cari_awal);

function cari_awal() {
    if (!map) {
        initializeMap();
    }
    fetchDataAndInitializeMap(`{{ asset('pasienterduga/data/all') }}`, 4);
}

function cari() {
    let selectedValue = document.getElementById("statuscek").value;

    var url;
    switch(selectedValue) {
        case '1':
            url = `{{ asset('pasienterduga/data/all') }}`;
            break;
        case '2':
            url = `{{ asset('pasien/data/all') }}`;
            break;
        default:
            url = `{{ asset('terduga/data/all') }}`;
    }
    fetchDataAndInitializeMap(url, selectedValue);
}

function fetchDataAndInitializeMap(url, selectedValue) {
    fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            clearMarkers();
            if (!map) {
                initializeMap();
            }
            addMarkers(map, data.data);
            const count = data.data.length;

            if(selectedValue == 1){
                console.log('1');
                const pasienData = data.data.filter(item => item.status === "Pasien");
                const countpasien = pasienData.length;
                document.getElementById('pasiencek').innerText = `${countpasien} (Pasien)`;

                const terdugaData = data.data.filter(item => item.status === "Terduga");
                const countterduga = terdugaData.length;
                document.getElementById('terdugacek').innerText = `${countterduga} (Terduga)`;
            }else if(selectedValue == 2){
                const pasienData = data.data.filter(item => item.status === "Pasien");
                const countpasien = pasienData.length;
                document.getElementById('pasiencek').innerText = `${countpasien} (Pasien)`;
                document.getElementById('terdugacek').innerText = '0';
            }
            else if(selectedValue == 3){
                const terdugaData = data.data.filter(item => item.status === "Terduga");
                const countterduga = terdugaData.length;
                document.getElementById('pasiencek').innerText = '0';
                document.getElementById('terdugacek').innerText = `${countterduga} (Terduga)`;
            }else{
                let pasienData = data.data.filter(item => item.status === "Pasien");
                let countpasien = pasienData.length;
                document.getElementById('pasiencek').innerText = `${countpasien} (Pasien)`;

                let terdugaData = data.data.filter(item => item.status === "Terduga");
                let countterduga = terdugaData.length;
                document.getElementById('terdugacek').innerText = `${countterduga} (Terduga)`;
            }

        })
        .catch(error => {
            console.error('Error fetching data:', error);
            // Display error message to the user
        });
}
function clearMarkers() {
    data = [];
    if (map) {
        map.eachLayer(function(layer) {
            if (layer instanceof L.Marker) {
                map.removeLayer(layer);
            }
        });
    }
}

function initializeMap() {
    map = L.map('map').setView([-6.2088, 106.8456], 12);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '© OpenStreetMap'
    }).addTo(map);
}

function addMarkers(map, coordinates) {
    coordinates.forEach(function(coord) {
        var marker = L.marker([coord.lat, coord.lng], {icon: getIcon(coord.color, coord.hasil_pengobatan)}).addTo(map);
        marker.bindPopup("Nama : " + coord.nama_pasien +
        "<br>Status : " + coord.status +
        "<br>Hasil Pengobatan : " + coord.hasil_pengobatan +
        "<br>Lokasi : (" + coord.lat + ", " + coord.lng + ")" +
        "<br>Faskes : "  + coord.pkm + "<br>Jarak Ke Faskes: "  + coord.jarak + " " + "Km").openPopup();
    });
}




// Function to get marker icon based on color
/*function getIcon(color) {
    return new L.Icon({
        iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-' + color + '.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });
}*/
function getIcon(color, hasil_pengobatan) {
    console.log(hasil_pengobatan);
    var iconUrl = '';

    if (hasil_pengobatan == 'Sembuh') {
        iconUrl = "{{ asset('green.png') }}";
    } else {
        iconUrl = `{{ asset('${color}.png') }}`;
    }

    return new L.Icon({
        iconUrl: iconUrl,
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-shadow.png',
        iconSize: [25, 25],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [20, 20]
    });
}


</script>

<script>
    let pins = [];
        $(document).ready(function() {
            // Fetch heatmap data
            $.ajax({
                url: `{{ asset('pasienterduga/data/heatmap') }}`,
                method: 'GET',
                dataType: 'json',
                success: function(response) {
                    var heatmapData = response.data.map(function(point) {
                        return [point[0], point[1], point[2]];
                    });

                    // Initialize the map
                    var mapHeat = L.map('mapheat');

                    // Add OpenStreetMap tiles
                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                        maxZoom: 10
                    }).addTo(mapHeat);

                    // Create and add heatmap layer
                    var heat = L.heatLayer(heatmapData, {
                        radius: 100, // Adjust radius as needed (100 meters for Monas)
                        blur: 10,    // Adjust blur as needed
                        maxZoom: 13, // Adjust maxZoom as needed
                        gradient: {
                            0.0: 'green',  // Lowest intensity
                            0.3: 'yellow', // Medium intensity
                            0.7: 'orange', // High intensity
                            1.0: 'red'     // Highest intensity (for Monas)
                        }
                    }).addTo(mapHeat);

                    // Get user's current location
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {
                            var userLat = position.coords.latitude;
                            var userLng = position.coords.longitude;

                            // Center map on user's location
                            mapHeat.setView([userLat, userLng], 12);

                            // Add marker at user's location
                            L.marker([userLat, userLng]).addTo(mapHeat).bindPopup('Lokasi Anda').openPopup();

                        }, function(error) {
                            console.error("Error getting location: ", error);
                            // Fallback to a default location (e.g., Jakarta) if location access is denied
                            mapHeat.setView([-6.2088, 106.8456], 12);
                        });
                    } else {
                        // Fallback to a default location (e.g., Jakarta) if Geolocation API is not supported
                        mapHeat.setView([-6.2088, 106.8456], 12);
                    }


                    pins.forEach(function(pin) {
                        L.marker([pin.lat, pin.lng]).addTo(mapHeat).bindPopup(pin.label);
                    });
                },
                error: function(xhr, status, error) {
                    console.error("Error fetching heatmap data:", error);
                }
            });
        });
    </script>


@endsection
