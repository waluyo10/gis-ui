<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="h-100" data-simplebar>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul id="side-menu">

                <li class="menu-title">User</li>

                <li>
                    <a href="{{route('second', ['profil', 'data'])}}">
                        <i data-feather="users"></i>
                        <span> Profil </span>
                    </a>
                </li>

                <li class="menu-title mt-2">Input Data</li>

                <li>
                    <a href="{{route('second', ['terduga', 'data'])}}">
                        <i data-feather="activity"></i>
                        <span> Terduga </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('second', ['pasien', 'data'])}}">
                        <i data-feather="list"></i>
                        <span> Pasien TB Ro</span>
                    </a>
                </li>
                <li class="menu-title mt-2">Laporan</li>

                <li>
                    <a href="{{route('second', ['peta', 'data'])}}">
                        <i data-feather="map"></i>
                        <span> Peta Sebaran Kasus</span>
                    </a>
                </li>
                <li class="menu-title mt-2">Master</li>

                <li>
                    <a href="{{route('second', ['master', 'data'])}}">
                        <i data-feather="file-text"></i>
                        <span> Data Users </span>
                    </a>
                </li>
                <li>
                    <a href="{{route('second', ['pkm', 'data'])}}">
                        <i data-feather="home"></i>
                        <span> Data Faskes </span>
                    </a>
                </li>

            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
