<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.shared.title-meta', ['title' => "Log In"])

    @include('layouts.shared.head-css')
</head>

<body class="authentication-bg authentication-bg-pattern">

    <div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div>
                    <div class="card bg-pattern">
                        <div class="card-body text-center">
                            <div class="text-center">
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/kemenkes.png')}}" alt="" height="80" style="margin-left: 2em;">
                                        </span>
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/dki.png')}}" alt="" height="80" style="margin-left: 2em;">
                                        </span>
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/germas.png')}}" alt="" height="80" style="margin-left: 2em;">
                                        </span>
                                        <span class="logo-lg">
                                            <img src="{{asset('assets/images/tosstb.png')}}" alt="" height="80" style="margin-left: 2em;">
                                        </span>

                                <h4 class="text-muted mb-4 mt-3">SISTEM INFORMASI PEMETAAN TUBERKULOSIS RESISTEN OBAT</h4>
                            </div>
                        </div> <!-- end card-body -->
                        <div class="text-center bg-soft-success">
                        <div class="form-group mb-1 ml-5 mr-5">
                        <form action="{{route('login')}}" method="POST" novalidate>
                                @csrf

                                <div class="form-group mb-3 text-center col-md-12">
                                    <label for="emailaddress"></label>
                                    <input class="form-control @if($errors->has('email')) is-invalid @endif" name="email" type="email" id="emailaddress" required="" value="{{ old('email')}}" placeholder="Enter your email" />

                                    @if($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group mb-3 col-md-12">
                                    <a href="{{route('login')}}" class="text-muted float-right"><small>Forgot your
                                            password?</small></a>
                                    <label for="password"></label>
                                    <div class="input-group input-group-merge @if($errors->has('password')) is-invalid @endif">
                                        <input class="form-control @if($errors->has('password')) is-invalid @endif" name="password" type="password" required="" id="password" placeholder="Enter your password" />
                                        <div class="input-group-append" data-password="false">
                                            <div class="input-group-text">
                                                <span class="password-eye"></span>
                                            </div>
                                        </div>
                                    </div>
                                    @if($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group mb-3 col-md-12">
                                    <div class="custom-control custom-checkbox text-left">
                                        <input type="checkbox" class="custom-control-input" id="checkbox-signin" checked>
                                        <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                                    </div>
                                </div>

                                <div class="form-group mb-3 col-md-12">
                                    <button class="btn btn-info btn-block text-center" type="submit"> Log In </button>
                                </div>

                            </form>
                        </div>
                        </div>
                    </div>
                    <!-- end card -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->


        <footer class="footer footer-alt">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy;  <a href="" class="text-white-50"></a>
        </footer>

        @include('layouts.shared.footer-script')

</body>

</html>
