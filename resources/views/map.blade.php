@extends('layouts.vertical', ['title' => 'Advanced Plugins'])

@section('css')
<!-- Add Leaflet CSS -->
   <link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css" />

<!-- Add Leaflet JS -->
<script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
@endsection

@section('content')
<div id="map" style="height: 500px;"></div>
@endsection

@section('script')
<script>
    var map = L.map('map').setView([51.505, -0.09], 13);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '© OpenStreetMap'
    }).addTo(map);

    var marker = L.marker([51.505, -0.09]).addTo(map);
    marker.bindPopup("<b>Hello world!</b><br>I am a popup.").openPopup();
</script>
@endsection
