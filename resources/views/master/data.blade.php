@extends('layouts.vertical', ['title' => 'Data Users'])
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet-draw/dist/leaflet.draw.css" />
@endsection

@section('content')
@php
$nama = session('name');
$email = session('email');
$profil = \App\User::where('email', $email)->first(); // Assuming you want to fetch a single
$role = $profil ? $profil->kat_id_user : null;

@endphp
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Menu</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Data Users</a></li>
                        </ol>
                    </div>
                    <h4 class="page-title">DATA USERS</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('success') }}
                    </div>
                    @endif
                    @if(session('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('error') }}
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="searchName">Pencarian Berdasarkan Nama :</label>
                        <input type="text" class="form-control col-6" id="searchName" placeholder="Nama">
                        <a href="{{route('second', ['master', 'data'])}}" class="btn btn-secondary waves-effect waves-light btn-sm mt-2"><span class="mdi mdi-refresh">Refresh</a>
                        <div class="col-md-12 mb-6">

                        </div>
                    </div>
                    <table id="demo-foo-filtering" class="table table-bordered toggle-circle mb-0" data-page-size="10" data-pagination="true">
                        <thead class="thead-light">
                        <tr>
                            <th data-toggle="true">NO</th>
                            <th>NAMA</th>
                            <th>EMAIL</th>
                            <th> KATEGORI USER </th>
                            <th class="text-center"> AKSI </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $index = 1; @endphp
                        @foreach($datas ?? [] as $data)
                        <tr>
                            <td>{{$index ++}}</td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->email}}</td>
                            <td>
                            @php
                                    $kategoriusers = \App\Models\KategoriUsers::where('id', $data->kat_id_user)->first();
                            @endphp
                            @if($data->kat_id_user == 1)
                                    <span class="badge badge-pink badge-pill">{{ $kategoriusers->kategori }} </span>
                                    @elseif($data->kat_id_user == 2)
                                    <span class="badge badge-info badge-pill">{{ $kategoriusers->kategori  }} </span>
                                    @elseif($data->kat_id_user == 3)
                                    <span class="badge badge-warning badge-pill">{{ $kategoriusers->kategori  }} </span>
                                    @else
                                   -
                                    @endif
                            </td>
                            <td class="text-center">
                            @if($role == '1' || $role == '2')
                            <a href="{{route('third', ['users', 'detail',  encrypt($data->id)])}}" class="jsgrid-button jsgrid-delete-button text-success text-center  mr-2 mb-2 mt-2"><span class="mdi mdi-pencil"></a>
                                <a href="{{route('second', ['usersdelete', encrypt($data->id)])}}" class="jsgrid-button jsgrid-delete-button text-danger text-center  mr-2 mb-2 mt-2"><span class="mdi mdi-delete"></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                                <tr class="active">
                                    <td colspan="10">
                                        <div class="text-right">
                                            <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10"></ul>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div id="con-close-modal-tambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h4 class="modal-title">Pengaturan Kategori Data User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                @foreach($userdetails ?? [] as $userdetail)
                <form action="{{ route('second', ['users', 'simpandata']) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body p-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                <input type="text" name="id" class="form-control" id="id" maxlength="150" placeholder="Module" required  value="{{$userdetail->id}}" hidden/>
                                    <label for="kategori" class="control-label">Kategori User<span class="text-danger">*</span></label>
                                    <select class="custom-select" id="kategori" name="kategori">
                                        @foreach($katUsers ?? [] as $katUser)
                                            <option value="{{ $katUser->id }}" @if($userdetail && $userdetail->kat_id_user == $katUser->id) selected @endif>
                                                {{ $katUser->kategori }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                    </div>
                </form>
            @endforeach




            </div>
        </div>
    </div><!-- /.modal -->
    </div> <!-- container -->

@endsection

@section('script')
    <!-- Plugins js-->
    <script src="{{asset('assets/libs/footable/footable.min.js')}}"></script>

    <!-- Page js-->
    <script src="{{asset('assets/js/pages/foo-tables.init.js')}}"></script>

    <script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-draw"></script>

@if(isset($showModal) && $showModal)
<script>
    $(document).ready(function() {
        $('#con-close-modal-tambah').modal('show');
    });
</script>
@endif

<script>
document.addEventListener("DOMContentLoaded", function() {
    const searchInput = document.getElementById('searchName');
    searchInput.addEventListener('keyup', function() {
        let filter = searchInput.value.toUpperCase();
        let table = document.getElementById("demo-foo-filtering");
        let tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (let i = 0; i < tr.length; i++) {
            let td = tr[i].getElementsByTagName("td")[1]; // Index 1 is the 'Nama' column
            if (td) {
                let textValue = td.textContent || td.innerText;
                if (textValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    });
});
</script>
@endsection
