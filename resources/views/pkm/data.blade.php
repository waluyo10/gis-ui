@extends('layouts.vertical', ['title' => 'Data Faskes'])
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet-draw/dist/leaflet.draw.css" />
@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Menu</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Data Faskes</a></li>
                        </ol>
                    </div>
                    <h4 class="page-title">DATA Faskes</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('success') }}
                    </div>
                    @endif
                    @if(session('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('error') }}
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="searchName">Pencarian Berdasarkan Nama PKM:</label>
                        <input type="text" class="form-control col-6" id="searchName" placeholder="Nama PKM">
                        <a href="{{route('second', ['master', 'data'])}}" class="btn btn-secondary waves-effect waves-light btn-sm mt-2"><span class="mdi mdi-refresh">Refresh</a>
                        <div class="col-md-12 mb-6">

                        </div>
                    </div>
                    <a href="{{route('second', ['pkm', 'form'])}}" type="button" class="btn btn-blue waves-effect waves-light float-right mr-2 mb-2 btn-sm" >
                        <i class="mdi mdi-plus-circle"></i> Tambah Data
                    </a>
                    <table id="demo-foo-filtering" class="table table-bordered toggle-circle mb-0" data-page-size="10" data-pagination="true">
                        <thead class="thead-light">
                        <tr>
                            <th data-toggle="true">NO</th>
                            <th>ID FASKES</th>
                            <th>NAMA FASKES</th>
                            <th>LAT</th>
                            <th>LONGT</th>
                            <th class="text-center"> AKSI </th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $index = 1; @endphp
                        @foreach($datas ?? [] as $data)
                        <tr>
                            <td>{{$index ++}}</td>
                            <td>{{$data->pkm_id}}</td>
                            <td>{{$data->pkm}}</td>
                            <td>{{$data->lat}}</td>
                            <td>{{$data->longt}}</td>
                            <td class="text-center">
                            <!--<a href="{{route('third', ['users', 'detail',  encrypt($data->id)])}}" class="jsgrid-button jsgrid-delete-button text-success text-center  mr-2 mb-2 mt-2"><span class="mdi mdi-pencil"></a>-->
                                <a href="{{route('second', ['pkmdelete' , encrypt($data->id)])}}" class="jsgrid-button jsgrid-delete-button text-danger text-center  mr-2 mb-2 mt-2"><span class="mdi mdi-delete"></a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                                <tr class="active">
                                    <td colspan="10">
                                        <div class="text-right">
                                            <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10"></ul>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div> <!-- container -->


@endsection

@section('script')
    <!-- Plugins js-->
    <script src="{{asset('assets/libs/footable/footable.min.js')}}"></script>

    <!-- Page js-->
    <script src="{{asset('assets/js/pages/foo-tables.init.js')}}"></script>

    <script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-draw"></script>

@if(isset($showModal) && $showModal)
<script>
    $(document).ready(function() {
        $('#con-close-modal-tambah').modal('show');
    });
</script>
@endif

<script>
document.addEventListener("DOMContentLoaded", function() {
    const searchInput = document.getElementById('searchName');
    searchInput.addEventListener('keyup', function() {
        let filter = searchInput.value.toUpperCase();
        let table = document.getElementById("demo-foo-filtering");
        let tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (let i = 0; i < tr.length; i++) {
            let td = tr[i].getElementsByTagName("td")[1]; // Index 1 is the 'Nama' column
            if (td) {
                let textValue = td.textContent || td.innerText;
                if (textValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    });
});
</script>
@endsection
