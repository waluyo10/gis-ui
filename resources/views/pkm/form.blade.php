@extends('layouts.vertical', ['title' => 'PKM'])
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet-draw/dist/leaflet.draw.css" />
@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Menu</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">PKM</a></li>
                        </ol>
                    </div>
                    <h4 class="page-title">Form PKM</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="header-title text-center">Form Tambah Data</h4>
                    <form id="myForm" action="{{route('second', ['pkm', 'simpandata'])}}" method="POST" enctype="multipart/form-data" >
                    @csrf
                    <div class="modal-body p-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">ID PKM<span class="text-danger">*</span></label>
                                    <input type="text" name="pkm_id" class="form-control" id="pkm_id" maxlength="250" placeholder="ID PKM" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">PKM<span class="text-danger">*</span></label>
                                    <input type="text" name="pkm" class="form-control" id="pkm" maxlength="250" placeholder="PKM" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">LAT<span class="text-danger">*</span></label>
                                    <input type="text" name="lat" class="form-control" id="lat" maxlength="250" placeholder="Lat" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">LONGT<span class="text-danger">*</span></label>
                                    <input type="text" name="longt" class="form-control" id="longt" maxlength="250" placeholder="Longt" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                                <div class="form-group">
                                <div id="map" style="height: 200px;"></div>
                                </div>
                        </div>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                    </div>

                </form>

            </div> <!-- end col -->
        </div>
        <!-- end row -->
        <!-- sample modal content -->




    </div> <!-- container -->

@endsection

@section('script')
    <!-- Plugins js-->
    <script src="{{asset('assets/libs/footable/footable.min.js')}}"></script>

    <!-- Page js-->
    <script src="{{asset('assets/js/pages/foo-tables.init.js')}}"></script>

    <script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-draw"></script>

    <script>
   var map = L.map('map').setView([-6.2088, 106.8456], 12);
 // Set view to DKI Jakarta

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
    }).addTo(map);

    var drawnItems = new L.FeatureGroup();
    map.addLayer(drawnItems);

    var drawControl = new L.Control.Draw({
        draw: {
            marker: true,
            circle: false,
            rectangle: false,
            polygon: false,
            polyline: false,
        },
        edit: {
            featureGroup: drawnItems,
            remove: true,
        },
    });
    map.addControl(drawControl);

    map.on(L.Draw.Event.CREATED, function (e) {
        var layer = e.layer;
        drawnItems.addLayer(layer);

        var latlng = layer.getLatLng();
        document.getElementById('lat').value = latlng.lat;
        document.getElementById('longt').value = latlng.lng;
    });
</script>





@endsection
