@extends('layouts.vertical', ['title' => 'Pasien TB Ro'])
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet-draw/dist/leaflet.draw.css" />
@endsection

@section('content')
@php
$nama = session('name');
$email = session('email');
$profil = \App\User::where('email', $email)->first(); // Assuming you want to fetch a single
$role = $profil ? $profil->kat_id_user : null;

@endphp
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Menu</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Pasien TB RO</a></li>
                        </ol>
                    </div>
                    <h4 class="page-title">Pasien TB RO</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <h4 class="header-title">Data</h4>
                    <p class="sub-header">
                        Berikut merupakan data daftar pasien TB RO
                    </p>
                    <div class="form-group">
                        <label for="searchName">Pencarian Berdasarkan Nama :</label>
                        <input type="text" class="form-control col-6" id="searchName" placeholder="Nama">
                        <a href="{{route('second', ['terduga', 'data'])}}" class="btn btn-secondary waves-effect waves-light btn-sm mt-2"><span class="mdi mdi-refresh">Refresh</a>
                        <div class="col-md-12 mb-6">

                        </div>
                    </div>
                    @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('success') }}
                    </div>
                    @endif
                    @if(session('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('error') }}
                    </div>
                    @endif
                    @if($role == '1' || $role == '2')
                    <button type="button" class="btn btn-pink waves-effect waves-light float-right mr-2 btn-sm mb-2" data-toggle="modal" data-target="#modal-import">
                        <i class="mdi mdi-import"></i> Import Data
                    </button>
                    @endif
                    <!--<a href="{{route('second', ['pasien', 'form'])}}" type="button" class="btn btn-blue waves-effect waves-light float-right mr-2 mb-2 btn-sm">
                        <i class="mdi mdi-plus-circle"></i> Tambah Data
                    </a>-->
                    <table id="demo-foo-row-toggler" class="table table-bordered toggle-circle mb-0" data-page-size="10" data-pagination="true">
                        <thead class="thead-light">
                        <tr>
                            <th data-toggle="true">NO</th>
                            <th>NAMA PASIEN </th>
                            <th>ALAMAT</th>
                            <th data-hide="all"> FASKES </th>
                            <th data-hide="all"> NAMA PETUGAS </th>
                            <th data-hide="all"> TEMPAT TANGGAL LAHIR </th>
                            <th data-hide="all"> USIA </th>
                            <th data-hide="all"> JENIS KELAMIN </th>
                            <th data-hide="all"> ALAMAT </th>
                            <th data-hide="all"> TGL DIAGNOSISI </th>
                            <th data-hide="all"> TGL MULAI PENGOBATAN </th>
                            <th data-hide="all"> HASIL PENGOBATAN </th>
                            <th data-hide="all"> NAMA KADER </th>
                            <th data-hide="all"> WILAYAH KADER </th>
                            <th data-hide="all"> LAT </th>
                            <th data-hide="all"> LONGT </th>
                            <th data-hide="all">AKSI</th>
                            <th>MASUKAN KORDINAT</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $index = 1; @endphp
                        @foreach($datas ?? [] as $data)
                        <tr>
                            <td>{{$index ++}}</td>
                            <td>{{$data->nama_pasien}}</td>
                            <td>{{$data->alamat}}</td>
                            <td>
                            @php
                                $pkms = \App\Models\Pkm::where('pkm_id', $data->id_faskes)->first();
                                if($pkms){
                                    echo $pkms->pkm;
                                }else{
                                    echo '-';
                                }
                            @endphp
                            </td>
                            <td>{{$data->nama_petugas}}</td>
                            <td>{{ $data->ttl}}</td>
                            <td>{{$data->usia}}</td>
                            <td>
                                @if($data->jenis_kelamin == 'L')
                                    {{ 'Laki - Laki' }}
                                @elseif($data->jenis_kelamin == 'P')
                                    {{ 'Perempuan' }}
                                @else
                                    {{ '-' }}
                                @endif
                            </td>
                            <td>{{$data->alamat}}</td>
                            <td>{{ $data->tgl_diagnosis }}</td>
                            <td>{{ $data->tgl_mulai_pengobatan }}</td>
                            <td>
                                <span class="badge label-table badge-success">{{$data->hasil_pengobatan}}</span>
                            </td>
                            <td>{{$data->nama_kader}}</td>
                            <td>{{$data->wilayah_kader}}</td>
                            <td>{{$data->lat}}</td>
                            <td>{{$data->longt}}</td>
                            <td class="text-center">
                            <a href="{{route('third', ['pasien', 'detail',  encrypt($data->id)])}}" class="jsgrid-button jsgrid-delete-button text-success text-center  mr-2 mb-2 mt-2"><span class="mdi mdi-pencil"></a>
                            @if($role == '1' || $role == '2')
                                <a href="{{route('second', ['pasiendelete' , encrypt($data->id)])}}" class="jsgrid-button jsgrid-delete-button text-danger text-center  mr-2 mb-2 mt-2"><span class="mdi mdi-delete"></a>
                                @endif
                            </td>
                            <td>
                            <a href="#" class="btn btn-primary waves-effect waves-light float-right mr-2 btn-sm mb-2"  data-toggle="modal" data-target="#modal-kordinat" data-id="{{($data->id)}}">
                                <i class="mdi mdi-map"></i>Titik Kordinat
                            </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                                <tr class="active">
                                    <td colspan="10">
                                        <div class="text-right">
                                            <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10"></ul>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                    </table>
                </div> <!-- end card-box -->
            </div> <!-- end col -->
        </div>
        <!-- end row -->
           <!-- 'Titik Kordinat' Modal -->
<div id="modal-kordinat" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalKordinatLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h4 class="modal-title" id="modalKordinatLabel">Titik Kordinat</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form id="kordinat" action="{{route('second', ['pasien', 'editdatatitik'])}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body p-4">
                        <!-- Alert Container -->
                        <div id="alert-container"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-10" class="control-label">Lat</label>
                                    <input type="text" name="id" class="form-control" id="id" placeholder="ID" hidden>
                                    <input type="text" name="lat_titik" class="form-control" id="lat_titik" placeholder="Lat" required>
                                </div>
                            </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label for="field-10" class="control-label">Longt</label>
                                    <input type="text" name="longt_titik" class="form-control" id="longt_titik" placeholder="Longt" required>
                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-10" class="control-label">Keterangan Detail</label>
                                    <textarea type="text" name="ket" class="form-control" id="ket" placeholder="Keterangan Detail" rows="4" readonly></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class=" modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light">Simpan Kordinat</button>
                    </div>
                </form>
        </div>
    </div>
</div>
<div id="modal-import" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h4 class="modal-title">Import Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form id="myForm" action="{{route('second', ['import', 'pasien'])}}" method="POST" enctype="multipart/form-data" >
                    @csrf
                    <div class="modal-body p-4">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Upload File Excel<span class="text-danger">*</span></label>
                                    <input type="file" name="file" class="form-control" id="file" maxlength="250" placeholder="FILE" required/>
                                </div>
                            </div>
                        </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light">Import Data</button>
                    </div>
                </form>

            </div>
        </div>
    </div><!-- /.modal -->

    </div> <!-- container -->


@endsection

@section('script')
    <!-- Plugins js-->
    <script src="{{asset('assets/libs/footable/footable.min.js')}}"></script>

    <!-- Page js-->
    <script src="{{asset('assets/js/pages/foo-tables.init.js')}}"></script>

    <script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-draw"></script>

    <script>
    $('#modal-kordinat').on('show.bs.modal', function(event) {
        event.stopPropagation(); // Prevent the modal from being shown before geolocation data is fetched
        var button = $(event.relatedTarget); // Button that triggered the modal
        var idTitik = button.data('id'); // Extract info from data-* attributes
        console.log(idTitik);
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var lat_titik = position.coords.latitude;
                var longt_titik = position.coords.longitude;
                console.log(lat_titik, longt_titik);
                fetch(`https://nominatim.openstreetmap.org/reverse.php?lat=${lat_titik}&lon=${longt_titik}&zoom=18&format=jsonv2`)
                    .then(response => response.json())
                    .then(data => {
                        $('#modal-kordinat').modal('show');

                        document.getElementById("lat_titik").value = data.lat;
                        document.getElementById("longt_titik").value = data.lon;
                        document.getElementById("ket").value = data.display_name;
                        document.getElementById("id").value = idTitik;
                        console.log(data);
                        console.log(idTitik);
                    })
                    .catch(error => {
                        // Handle errors
                        console.error(error);
                    });
            });
        }
    });
</script>


<script>
document.addEventListener("DOMContentLoaded", function() {
    const searchInput = document.getElementById('searchName');
    searchInput.addEventListener('keyup', function() {
        let filter = searchInput.value.toUpperCase();
        let table = document.getElementById("demo-foo-row-toggler");
        let tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (let i = 0; i < tr.length; i++) {
            let td = tr[i].getElementsByTagName("td")[1]; // Index 1 is the 'Nama' column
            if (td) {
                let textValue = td.textContent || td.innerText;
                if (textValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    });
});
</script>




@endsection
