<!DOCTYPE html>
<html >

<head>
    <meta charset="utf-8" />
    <title>GEOGRAPHIC INFORMATION SYSTEMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />

    <!--Material Icon -->
    <link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />

    <!-- Custom  sCss -->
    <link href="{{asset('assets/css/landing.min.css')}}" rel="stylesheet" type="text/css" />

    <!--Map-->
    <!-- Add Leaflet CSS -->
   <link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css" />

    <!-- Add Leaflet JS -->
    <script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>


</head>

<body>

    <!--Navbar Start-->
    <nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-dark">
        <div class="container-fluid">
            <!-- LOGO -->
            <a class="logo text-uppercase" href="index.html">
                <img src="{{asset('assets/images/kemenkes.png')}}" alt="" class="logo-light" height="45" />
                <img src="{{asset('assets/images/kemenkes.png')}}" alt="" class="logo-dark" height="45" />
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="mdi mdi-menu"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mx-auto navbar-center" id="mySidenav">
                    <li class="nav-item active">
                        <a href="#home" class="nav-link"></a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto navbar-center">
                    <li class="nav-item">
                        <a href="{{route('second', ['auth', 'login'])}}" class="btn btn-info navbar-btn ml-2">Masuk</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Navbar End -->

    <!-- home start -->
    <section id="home" >
    <div class="text-center">
    <h3 class="text-primary">Sistem Informasi Pemetaan Tuberkulosis Resisten Obat</h3>
        <h5 class="text-muted">(SIPETATBRO)</h5>
    </div>
        <div class="home-center">
            <div class="home-desc-center bg-soft-secondary">
                <div class="container-fluid">
                    <div class="card-box mt-1">
                        <div class="mb-2">
                        <div class="card-body">
                            <div class="text-info bg-soft-info">
                            <h4 class="text-info text-center pt-4 pb-4">
                            <img src="{{asset('map.png')}}" alt="user-image" class="rounded-circle" height="40">
                            DASHBOARD</h4>
                            </div>

        <form id="search-form" action="{{route('second', ['tasksall', 'pencarian'])}}" method="POST" onsubmit="showLoadingModal()">
            @csrf
            <div class="form-row">

                <div class="col-md-6 mb-2">
                    <label for="field-1" class="control-label">Nama Faskes</label>
                    <select class="custom-select " id="status" name="status" onchange="cari()">
                       <option value="0">Semua</option>
                       @foreach($Pkms ?? [] as $Pkm)
                                            <option value="{{ $Pkm->pkm_id }}">
                                                {{ $Pkm->pkm }}
                                            </option>
                                        @endforeach
                    </select>
                </div>
                <!--<div class="col-md-6 mb-2">
                                <label for="field-3" class="control-label">Wilayah<span class="text-danger"></span></label>
                                <select class="custom-select" id="wilayah" name="wilayah" onchange="cari()">
                                    <option value="1">Jakarta</option>
                                </select>
                            </div>
-->
                <div class="col-md-12 mb-2">
                    <!--<button type="submit" class="btn btn-info waves-effect waves-light btn-sm"><i class="mdi mdi-book-search-outline"></i>Search</button>-->
                    <a href="{{route('index')}}" class="btn btn-secondary waves-effect waves-light btn-sm"><span class="mdi mdi-refresh">Refresh</a>

                </div>

            </div>
        </form>
        <hr>
        <div class="form-row">

            <div class="col-md-6 mb-2">
            <div class="bg-blue text-white text-center">
            <div id="terduga"></div>
            </div>
            </div>
            <div class="col-md-6 mb-2">
            <div class="bg-danger text-white text-center">
                <div id="pasien"></div>
            </div>
            </div>
            </div>

    </div>
                        </div>
                    <div id="map" style="height: 500px;"></div>
                    <div><b>Keterangan :</b></div>
                    <ul class="sortable-list tasklist list-unstyled">
                    <p><i class="mdi mdi-circle text-blue"></i> <span>Terduga</span></p>
                        <p><i class="mdi mdi-circle text-danger"></i> <span>Pasien</span></p>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- home end -->


    <!-- footer start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row mb-0">

            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="float-left pull-none">
                        <p class="">SIPETATBRO <a href="https://sipetatbro.com/" target="_blank" class="">sipetatbro.com</a> </p>
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </footer>
    <!-- footer end -->

    <!-- Back to top -->
    <a href="#" class="back-to-top" id="back-to-top"> <i class="mdi mdi-chevron-up"> </i> </a>

    <!-- javascript -->
    <script src="{{asset('assets/js/vendor.min.js')}}"></script>

    <!-- custom js -->
    <script type="text/javascript">
        ! function($) {
            "use strict";

            var Ubold = function() {};

            Ubold.prototype.initStickyMenu = function() {
                    $(window).scroll(function() {
                        var scroll = $(window).scrollTop();

                        if (scroll >= 50) {
                            $(".sticky").addClass("nav-sticky");
                        } else {
                            $(".sticky").removeClass("nav-sticky");
                        }
                    });
                },

                Ubold.prototype.initSmoothLink = function() {
                    $('.navbar-nav a').on('click', function(event) {
                        var $anchor = $(this);
                        $('html, body').stop().animate({
                            scrollTop: $($anchor.attr('href')).offset().top - 50
                        }, 1500);
                        event.preventDefault();
                    });

                    // general
                    $("a.smooth-scroll").on('click', function(e) {
                        e.preventDefault();
                        var dest = $(this).attr('href');
                        $('html,body').animate({
                            scrollTop: $(dest).offset().top
                        }, 'slow');
                    });
                },


                Ubold.prototype.initBacktoTop = function() {
                    $(window).scroll(function() {
                        if ($(this).scrollTop() > 100) {
                            $('.back-to-top').fadeIn();
                        } else {
                            $('.back-to-top').fadeOut();
                        }
                    });
                    $('.back-to-top').click(function() {
                        $("html, body").animate({
                            scrollTop: 0
                        }, 1000);
                        return false;
                    });
                },


                Ubold.prototype.init = function() {
                    this.initStickyMenu();
                    this.initSmoothLink();
                    this.initBacktoTop();
                },
                //init
                $.Ubold = new Ubold, $.Ubold.Constructor = Ubold
        }(window.jQuery),

        //initializing
        function($) {
            "use strict";
            $.Ubold.init();
        }(window.jQuery);
    </script>
<script>
var map = null; // Declare map variable outside the functions

document.addEventListener('DOMContentLoaded', cari_awal);

function cari_awal() {
    if (!map) {
        initializeMap();
    }

    fetchDataAndInitializeMap(`{{ asset('pkm/data/all') }}`, 4);
}

function cari() {
    var selectedValue = document.getElementById("status").value;

    var url;
    if(selectedValue == '0'){
        url =`{{ asset('pkm/data/all') }}`;
    }else{
        url = `{{ asset('pkm/caridata/${selectedValue}') }}`;
    }

    fetchDataAndInitializeMap(url);
}

let totalPasien = 0;
let totalTerduga = 0;
function fetchDataAndInitializeMap(url) {
    fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            clearMarkers();
            if (!map) {
                initializeMap();
            }
            addMarkers(map, data.data);
            const count = data.data.length;
            data.data.forEach(function(item) {
            // Add the value of total_pasien from each item to totalPasien
            totalPasien += item.total_pasien;
            totalTerduga += item.total_terduga;
            });
            //console.log(totalPasien)
            document.getElementById('pasien').innerText = `${totalPasien} (Pasien)`;
            document.getElementById('terduga').innerText = `${totalTerduga} (Terduga)`;

        })
        .catch(error => {
            console.error('Error fetching data:', error);
            // Display error message to the user
        });
}
function clearMarkers() {
    data = [];
    totalPasien = 0;
    totalTerduga = 0;
    if (map) {
        map.eachLayer(function(layer) {
            if (layer instanceof L.Marker) {
                map.removeLayer(layer);
            }
        });
    }
}

function initializeMap() {
    map = L.map('map').setView([-6.2088, 106.8456], 12);
    //map = L.map('map').setView([-2.4833826, 117.8902853], 4); // Centered on Indonesia
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '© OpenStreetMap'
    }).addTo(map);
}

function addMarkers(map, coordinates) {
    coordinates.forEach(function(coord) {
        var marker = L.marker([coord.lat, coord.lng], {icon: getIcon(coord.color)}).addTo(map);
        marker.bindPopup("<span class='text-primary'><b>" + coord.pkm  + "</b></span>" +
        "<br>Jumlah Terduga : " + coord.total_terduga +
        "<br>Jumlah Pasien : " + coord.total_pasien +
        "<br>Pasien Sembuh : " + coord.total_sembuh +
        "<br>Persentase Sembuh : " + coord.persentase_sembuh + "%" +
        "<br>Investigasi Kontak : - "  +
        "<br>Lokasi : (" + coord.lat + ", " + coord.lng + ")").openPopup();
    });
}

// Function to get marker icon based on color
function getIcon(color) {
    return new L.Icon({
        iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-blue.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });
}

</script>

<script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
</body>

</html>
