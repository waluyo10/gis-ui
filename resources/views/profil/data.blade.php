@extends('layouts.vertical', ['title' => 'Profil'])
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet-draw/dist/leaflet.draw.css" />
@endsection

@section('content')
@php
$nama = session('name');
$email = session('email');
$profil = \App\User::where('email', $email)->first(); // Assuming you want to fetch a single
$role = $profil ? $profil->kat_id_user : null;

@endphp
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Menu</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Profil</a></li>
                        </ol>
                    </div>
                    <h4 class="page-title">Profil</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
        <div class="col-sm-12">
        @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('success') }}
                    </div>
                    @endif
                    @if(session('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('error') }}
                    </div>
                    @endif
        </div>
            <div class="col-sm-4">
                <div class="card-box text-center bg-soft-info">
                <img src="{{asset('default.png')}}" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
                <h4 class="mb-0">
                    {{$nama}}
                </h4>
                    <p class="text-muted">{{ $email }}</p>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="card-box">
                <h5 class="text-uppercase mt-0 mb-3 bg-primary p-2 text-center text-white">Daftar Petugas</h5>
                <div class="form-group">
                        <label for="searchName">Pencarian Berdasarkan Nama Petugas :</label>
                        <input type="text" class="form-control col-6" id="searchName" placeholder="Nama">
                        <a href="{{route('second', ['profil', 'data'])}}" class="btn btn-secondary waves-effect waves-light btn-sm mt-2"><span class="mdi mdi-refresh">Refresh</a>
                        <div class="col-md-12 mb-6">

                        </div>
                    </div>

                @if($role == '1' || $role == '2')
                <a href="#" type="button" class="btn btn-blue waves-effect waves-light float-right mr-2 mb-2 btn-sm" data-toggle="modal" data-target="#con-close-modal-tambah-petugas">
                        <i class="mdi mdi-plus-circle"></i> Tambah Data Petugas
                    </a>
                @endif
                    <table id="demo-foo-filtering" class="table table-bordered toggle-circle mb-0" data-page-size="10" data-pagination="true">
                        <thead class="thead-light">
                        <tr>
                            <th data-toggle="true">NO</th>
                            <th>PKM</th>
                            <th>NAMA PETUGAS</th>
                            <th>EMAIL</th>
                            <th>AKSI</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $index = 1; @endphp
                        @foreach($datapetugass ?? [] as $datapetugas)
                        <tr>
                            <td>{{$index ++}}</td>
                            <td>
                            @php
                                $pkms = \App\Models\Pkm::where('pkm_id', $datapetugas->pkm)->first();
                                if($pkms){
                                    echo $pkms->pkm;
                                }else{
                                    echo '-';
                                }
                            @endphp
                            </td>
                            <td>{{$datapetugas->nama_petugas}}</td>
                            <td>{{$datapetugas->email}}</td>
                            <td class="text-center"> <a href="{{route('second', ['petugasdelete' , encrypt($datapetugas->id)])}}" class="jsgrid-button jsgrid-delete-button text-danger text-center  mr-2 mb-2 mt-2"><span class="mdi mdi-delete"></a>
                        </td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                                <tr class="active">
                                    <td colspan="10">
                                        <div class="text-right">
                                            <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10"></ul>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
        <div class="col-sm-12">
                <div class="card-box">
                <h5 class="text-uppercase mt-0 mb-3 bg-primary p-2 text-center text-white">Daftar Kader</h5>
                @if($role == '1' || $role == '2')
                <a href="#" type="button" class="btn btn-blue waves-effect waves-light float-right mr-2 mb-2 btn-sm" data-toggle="modal" data-target="#con-close-modal-tambah">
                        <i class="mdi mdi-plus-circle"></i> Tambah Data Kader
                    </a>
                    @endif

                    <table id="demo-foo-filtering-kader" class="table table-bordered toggle-circle mb-0" data-page-size="10" data-pagination="true">
                        <thead class="thead-light">
                        <tr>
                            <th data-toggle="true">NO</th>
                            <th>NAMA KADER </th>
                            <th>EMAIL </th>
                            <th>WILAYAH KADER</th>
                            <th>AKSI</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $index = 1; @endphp
                        @foreach($datas ?? [] as $data)
                        <tr>
                            <td>{{$index ++}}</td>
                            <td>{{$data->nama_kader}}</td>
                            <td>{{$data->email}}</td>
                            <td>{{$data->wilayah}}</td>
                            <td class="text-center"> <a href="{{route('second', ['kaderdelete' , encrypt($data->id)])}}" class="jsgrid-button jsgrid-delete-button text-danger text-center  mr-2 mb-2 mt-2"><span class="mdi mdi-delete"></a>
                        </td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                                <tr class="active">
                                    <td colspan="10">
                                        <div class="text-right">
                                            <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10"></ul>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div id="con-close-modal-tambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h4 class="modal-title">Tambah Data Kader</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form id="myForm" action="{{route('second', ['kader', 'simpandata'])}}" method="POST" enctype="multipart/form-data" >
                    @csrf
                    <div class="modal-body p-4">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Nama Kader<span class="text-danger">*</span></label>
                                    <input type="text" name="nama_kader" class="form-control" id="nama_kader" maxlength="250" placeholder="Nama Kader" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Email Kader<span class="text-danger">*</span></label>
                                    <input type="email" class ="form-control" name="email" id="email" placeholder="Email" rows="3" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Wilayah Kader<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="wilayah" id="wilayah" placeholder="Wilayah" rows="3" required></textarea>
                                </div>
                            </div>
                        </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                    </div>
                </form>

            </div>
        </div>
    </div><!-- /.modal -->
    </div> <!-- container -->
        <!-- end row -->

    <div id="con-close-modal-tambah-petugas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-light">
                    <h4 class="modal-title">Tambah Data Petugas</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form id="myForm" action="{{route('second', ['petugas', 'simpandata'])}}" method="POST" enctype="multipart/form-data" >
                    @csrf
                    <div class="modal-body p-4">

                        <div class="row">
                        <div class="col-md-12">
                        <div class="form-group">
                                    <label for="kategori" class="control-label">PKM<span class="text-danger">*</span></label>
                                    <select class="custom-select" id="pkm" name="pkm" required>
                                    <option value="">--Pilih--</option>
                                        @php
                                        $pkms = \App\Models\Pkm::orderBy('pkm', 'asc')->get();

                                        if ($pkms->isNotEmpty()) {
                                        foreach ($pkms as $pkm) {
                                        echo '<option value="' . $pkm->pkm_id . '">' . $pkm->pkm . '</option>';
                                        }
                                        } else {
                                        echo '<option value="-">-</option>';
                                        }
                                        @endphp
                                    </select>
                                </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Nama Petugas<span class="text-danger">*</span></label>
                                    <input type="text" name="nama_petugas" class="form-control" id="nama_Petugas" maxlength="250" placeholder="Nama Petugas" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Email Petugas<span class="text-danger">*</span></label>
                                    <input type="email" class ="form-control" name="email" id="email" placeholder="Email" rows="3" required>
                                </div>
                            </div>
                        </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-info waves-effect waves-light">Simpan</button>
                    </div>
                </form>

            </div>
        </div>
    </div><!-- /.modal -->

@endsection

@section('script')
    <!-- Plugins js-->
    <script src="{{asset('assets/libs/footable/footable.min.js')}}"></script>

    <!-- Page js-->
    <script src="{{asset('assets/js/pages/foo-tables.init.js')}}"></script>

    <script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-draw"></script>

    <script>
document.addEventListener("DOMContentLoaded", function() {
    const searchInput = document.getElementById('searchName');
    searchInput.addEventListener('keyup', function() {
        let filter = searchInput.value.toUpperCase();
        let table = document.getElementById("demo-foo-filtering");
        let tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (let i = 0; i < tr.length; i++) {
            let td = tr[i].getElementsByTagName("td")[1]; // Index 1 is the 'Nama' column
            if (td) {
                let textValue = td.textContent || td.innerText;
                if (textValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    });
});
</script>
@endsection
